package com.terkaly;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;


import org.json.JSONException;

@Path("azurecourse")
public class RestTutorial {

 private Map < String, AzureCourse > map = new HashMap < String, AzureCourse > ();

 public RestTutorial() {
  AzureCourse azureCourse = new AzureCourse("100", "Building REST in a Container");
  azureCourse.setNotes("REST in Mesosphere");
  map.put(azureCourse.getCourseNumber(), azureCourse);
  AzureCourse azureCourse2 = new AzureCourse("101", "Mesosphere and Orchestration");
  azureCourse.setNotes("Run your containers");
  map.put(azureCourse2.getCourseNumber(), azureCourse2);
 }

 // Execute with http://localhost:8080/SimpleRestfulService/rest/azurecourse/query?code=100
 @GET
 @Path("query")
 @Produces("application/json")
 public ArrayList < AzureCourse > getData(@QueryParam("course") String course) throws JSONException {
  try {
   ArrayList < AzureCourse > list = new ArrayList < AzureCourse > ();
   AzureCourse result = (AzureCourse) map.get(course);
   list.add(result);
   return list;

  } catch (Throwable t) {
   System.out.println(t.getMessage());
  }
  return null;
 }
}
