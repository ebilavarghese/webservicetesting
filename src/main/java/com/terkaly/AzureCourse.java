package com.terkaly;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AzureCourse {

    public AzureCourse() { }
    
    public AzureCourse(String courseNumber, String courseTitle) {
        this.CourseNumber = courseNumber;
        this.CourseTitle = courseTitle;
    }
    
    // Make sure it is private
    private String CourseNumber;
    private String CourseTitle;
    private String Notes;
    
    // Jersey tooling uses getter methods
    public String getCourseNumber() {
		return CourseNumber;
	}

	public void setCourseNumber(String courseNumber) {
		CourseNumber = courseNumber;
	}

	public String getCourseTitle() {
		return CourseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		CourseTitle = courseTitle;
	}

	public String getNotes() {
        return this.Notes;
    }
    
    public void setNotes(String notes) {
        this.Notes = notes;
    }
	@Override
	public String toString() {
		return "AzureCourse [courseNumber=" + CourseNumber + ", courseTitle=" + CourseTitle + "]";
	}
	
}
